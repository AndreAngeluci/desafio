install: 
	pip install -r requirements.txt

test:
	python -m unittest discover -s tests

build:
	docker build -t orama_freelancer .
	
run:
	docker run -d -p 5000:5000 --name orama_frelancer_api orama_freelancer

stop:
	docker stop -t 0 orama_frelancer_api
	docker rm orama_frelancer_api