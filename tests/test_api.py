import unittest
import json

from app import app


class APITest(unittest.TestCase):

    def setUp(self):
        self.api_test_client = app.test_client()
        self.valid_payload = {
            "freelance": {
                "id": 42,
                "user": {
                    "firstName": "Hunter",
                    "lastName": "Moore",
                    "jobTitle": "Fullstack JS Developer"
                },
                "status": "new",
                "retribution": 650,
                "availabilityDate": "2018-06-13T00:00:00+01:00",
                "professionalExperiences": [
                    {
                        "id": 4,
                        "companyName": "Okuneva, Kerluke and Strosin",
                        "startDate": "2016-01-01T00:00:00+01:00",
                        "endDate": "2018-05-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 241,
                                "name": "React"
                            },
                            {
                                "id": 270,
                                "name": "Node.js"
                            },
                            {
                                "id": 370,
                                "name": "Javascript"
                            }
                        ]
                    },
                    {
                        "id": 54,
                        "companyName": "Hayes - Veum",
                        "startDate": "2014-01-01T00:00:00+01:00",
                        "endDate": "2016-09-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 470,
                                "name": "MySQL"
                            },
                            {
                                "id": 400,
                                "name": "Java"
                            },
                            {
                                "id": 370,
                                "name": "Javascript"
                            }
                        ]
                    },
                    {
                        "id": 80,
                        "companyName": "Harber, Kirlin and Thompson",
                        "startDate": "2013-05-01T00:00:00+01:00",
                        "endDate": "2014-07-01T00:00:00+01:00",
                        "skills": [
                            {
                                "id": 370,
                                "name": "Javascript"
                            },
                            {
                                "id": 400,
                                "name": "Java"
                            }
                        ]
                    }
                ]
            }
        }

    def test_api_valid_payload(self):
        response = self.api_test_client.post(
            "/", headers={"Content-Type": "application/json"},
            data=json.dumps(self.valid_payload))
        self.assertEqual(200, response.status_code)

    def test_api_unprocessable_entity(self):
        invalid_payload = "{}"
        response = self.api_test_client.post(
            "/", headers={"Content-Type": "application/json"},
            data=invalid_payload)
        self.assertEqual(422, response.status_code)
