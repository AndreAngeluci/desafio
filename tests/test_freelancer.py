import unittest
from freelancer import Freelancer


class FreelancerTest(unittest.TestCase):

    def setUp(self):
        # create default freelancer dictionary
        self.freelancer_dict = {
            "id": 0,
            "professionalExperiences": []
        }
        self.experience_default_list = [
            {
                "id": 0,
                "startDate": "",
                "endDate": "",
                "skills": [
                    {
                        "id": 1,
                        "name": "Python"
                    }
                ]
            }, {
                "id": 0,
                "startDate": "",
                "endDate": "",
                "skills": [
                    {
                        "id": 1,
                        "name": "Python"
                    }
                ]
            }
        ]

    def test_freelancer_skills_dates_without_overlapping(self):
        experiences_list = list(self.experience_default_list)
        experiences_list[0]["startDate"] = "2016-01-01T00:00:00+01:00"
        experiences_list[0]["endDate"] = "2017-01-01T00:00:00+01:00"
        experiences_list[1]["startDate"] = "2018-01-01T00:00:00+01:00"
        experiences_list[1]["endDate"] = "2019-01-01T00:00:00+01:00"
        self.freelancer_dict["professionalExperiences"] = experiences_list
        freelancer = Freelancer(**self.freelancer_dict)
        result_dict = freelancer.months_experience_skills()
        duration_months = sum([
            skill["durationInMonths"] for skill in
            result_dict["freelance"]["computedSkills"]])
        self.assertEqual(duration_months, 24)

    def test_freelancer_skills_dates_with_overlapping(self):
        experiences_list = list(self.experience_default_list)
        experiences_list[0]["startDate"] = "2016-01-01T00:00:00+01:00"
        experiences_list[0]["endDate"] = "2017-01-01T00:00:00+01:00"
        experiences_list[1]["startDate"] = "2015-07-01T00:00:00+01:00"
        experiences_list[1]["endDate"] = "2016-07-01T00:00:00+01:00"
        self.freelancer_dict["professionalExperiences"] = experiences_list
        freelancer = Freelancer(**self.freelancer_dict)
        result_dict = freelancer.months_experience_skills()
        duration_months = sum([
            skill["durationInMonths"] for skill in
            result_dict["freelance"]["computedSkills"]])
        self.assertEqual(duration_months, 18)

    def test_freelancer_skills_duplicated_dates(self):
        experiences_list = list(self.experience_default_list)
        experiences_list[0]["startDate"] = "2016-01-01T00:00:00+01:00"
        experiences_list[0]["endDate"] = "2017-01-01T00:00:00+01:00"
        experiences_list[1]["startDate"] = "2016-01-01T00:00:00+01:00"
        experiences_list[1]["endDate"] = "2017-01-01T00:00:00+01:00"
        self.freelancer_dict["professionalExperiences"] = experiences_list
        freelancer = Freelancer(**self.freelancer_dict)
        result_dict = freelancer.months_experience_skills()
        duration_months = sum([
            skill["durationInMonths"] for skill in
            result_dict["freelance"]["computedSkills"]])
        self.assertEqual(duration_months, 12)

    def test_freelancer_skills_nested_dates(self):
        experiences_list = list(self.experience_default_list)
        experiences_list[0]["startDate"] = "2016-01-01T00:00:00+01:00"
        experiences_list[0]["endDate"] = "2017-01-01T00:00:00+01:00"
        experiences_list[1]["startDate"] = "2016-03-01T00:00:00+01:00"
        experiences_list[1]["endDate"] = "2016-06-01T00:00:00+01:00"
        self.freelancer_dict["professionalExperiences"] = experiences_list
        freelancer = Freelancer(**self.freelancer_dict)
        result_dict = freelancer.months_experience_skills()
        duration_months = sum([
            skill["durationInMonths"] for skill in
            result_dict["freelance"]["computedSkills"]])
        self.assertEqual(duration_months, 12)
