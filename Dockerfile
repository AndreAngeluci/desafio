FROM python:3.8

WORKDIR /app
EXPOSE 5000/tcp

RUN pip install waitress
COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY app.py /app/
COPY freelancer/*.py /app/freelancer/
CMD ["waitress-serve", "--port=5000", "app:app"]