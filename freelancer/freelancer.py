from datetime import datetime


class Skill:
    """
        Skill class with its name and a set with
        all days between start and end dates of experience
    """
    def __init__(self, name):
        self.name = name
        self.days_experience = set()


class Freelancer:
    def __init__(self, **args):
        self.__dict__.update(args)
        self.computed_skills = dict()
        self.process_freelancer()

    def process_freelancer(self):
        """ Process the freelancer data to compute your skills """
        for experiences in self.professionalExperiences:
            for skill in experiences["skills"]:
                start_date = datetime.fromisoformat(experiences["startDate"])
                end_date = datetime.fromisoformat(experiences["endDate"])
                self.compute_skill(
                    skill["id"], skill["name"],
                    (start_date, end_date))

    def compute_skill(
        self, skill_id, skill_name,
        start_end_date: tuple
    ):
        """
            Computes the skills experience

            Parameters:
                skill_id (int): Id of the skill
                skill_name (str): Name of the skill
                start_end_date (tuple): Tuple with the first and last skill
                    experience date
        """
        try:
            # check if skill already exists
            skill = self.computed_skills[skill_id]
        except KeyError:
            # create the new skill object.
            # the freelancer experience days are allocated in a set.
            # sets cannot have duplicate values, this avoid the
            # overlapping dates problem
            skill = Skill(skill_name)
            self.computed_skills.update({skill_id: skill})
        start_date, end_date = start_end_date
        # extract all days between first and last dates and update the set
        skill.days_experience.update(
            set(datetime.fromordinal(date) for date in range(
                start_date.toordinal(), end_date.toordinal())))

    def months_experience_skills(self) -> dict:
        """Return the months count of experience by freelancer skill"""
        MONTH_DAYS = 30
        skills_with_months = list()
        for skill in self.computed_skills:
            # calcutes skill experience in months
            skill_duration_months = len(
                self.computed_skills[skill].days_experience) // MONTH_DAYS
            skills_with_months.append({
                "id": skill,
                "name": self.computed_skills[skill].name,
                "durationInMonths": skill_duration_months
            })
        return {"freelance": {
            "id": self.id,
            "computedSkills": skills_with_months
        }}
