from flask import Flask, Response, request
from freelancer import Freelancer


app = Flask("freelancer_api")
app.config["JSON_SORT_KEYS"] = False


@app.route('/', methods=['POST'])
def calc_freelancer_experience():
    """
        Receives a freelancer JSON e returns your skills
        experience in months
    """
    try:
        freelancer = Freelancer(**request.get_json()['freelance'])
        return freelancer.months_experience_skills()
    except Exception as err:
        print("Error", err)
        return Response(status=422)


if __name__ == "__main__":
    app.run(port=5000)
